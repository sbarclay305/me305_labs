#Fibonacci Numbers lab0 - calculates fibonacci numbers given an index
#Soren Barclay
#ME 305
#9.23.21

def fib(idx):   #Function that calculates the fibonacci number given an index
   fibseq = [0, 1]
   counter = 2
   while counter <= idx: #Adds prior two terms to the final term until the index is reached
       fibseq.append(fibseq[-2] + fibseq[-1])
       counter += 1
   return fibseq[-1]       

if __name__ == '__main__':
    #Declaring variables
    errormsg = "Try again, you must input a positive integer. For example: 45000 "
    runp = input("Press enter to start or q to quit")
    
    while runp != 'q': #Run the code if the user presses enter
        idx = int(input("Enter a positive integer "))   
        
        try:
            if (idx < 0): 
                  print(errormsg)
            else:            
                print("the Fibonacci number at index ", idx, "is ", fib(idx))
        except: 
            print(errormsg)
    
        runp = input("Press any key to start or q to quit") #reiterates beginning prompt

